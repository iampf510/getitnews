#!/usr/bin/env python
import datetime

import requests
from bs4 import BeautifulSoup

from news.config import ARTICLE_PLUS, UPLOAD_PASSWORD, WEBSITE_URL


class NewsBot:
    def __init__(self):
        self.websites = [
                {'website':'https://segmentfault.com',
                 'article':'.news-item',
                 'title':'.news__item-title'},
                {'website':'https://www.csdn.net',
                 'article':'.list_con',
                 'title':'h2 a',
                 'url':'h2 a'},
                {'website':'https://blog.techbridge.cc',
                 'article':'.post-title'},
                #{'website':'https://juejin.im',
                # 'article':'.title-row',
                # 'title':'a',
                # 'url':'a'},
                {'website':'https://jaceju.net',
                 'article':'.post'},
                {'website':'https://www.ithome.com.tw',
                 'article':'.title'},
                {'website':'https://www.inside.com.tw',
                 'article':'.post_title'},
                {'website':'https://buzzorange.com/techorange',
                 'article':'article',
                 'title':'h4 a',
                 'url':'h4 a'},
                {'website':'https://blog.fundebug.com',
                 'article':'article',
                 'title':'h1 a',
                 'url':'h1 a'},
                {'website':'https://www.bnext.com.tw',
                 'article':'.item_text_box'},
                {'website':'https://www.ifanr.com',
                 'article':'.article-item',
                 'title':'h3 a',
                 'url':'h3 a'},
                {'website':'http://blog.jobbole.com/all-posts',
                 'article':'.post',
                 'title':'p a',
                 'url':'p a'},
                {'website':'https://www.oschina.net',
                 'article':'div.vertical.news'},
                {'website':'https://www.cnblogs.com',
                 'article':'.post_item',
                 'title':'h3 a',
                 'url':'h3 a'},
                {'website':'http://www.codeceo.com',
                 'article':'article',
                 'title':'h3 a',
                 'url':'h3 a'},
                {'website':'https://toutiao.io',
                 'article':'.post',
                 'title':'h3 a',
                 'url':'h3 a'},
                {'website':'https://www.iteye.com',
                 'article':'.title'},


                {'website':'https://www.cw.com.tw/masterChannel.action?idMasterChannel=8',
                 'article':'.article',
                 'title':'h3 a',
                 'url':'h3 a',
                 'category':'fin'},
                {'website':'https://tw.news.yahoo.com/finance',
                 'article':'.Cf',
                 'title':'h3 a',
                 'url':'h3 a',
                 'category':'fin'},
                {'website':'https://www.bbc.com/zhongwen/trad/business',
                 'article':'.eagle-item__body',
                 'category':'fin'},
                {'website':'http://www.epochtimes.com/b5/news420.htm',
                 'article':'.posts',
                 'category':'fin'},
                {'website':'https://www.ettoday.net/news/focus/%E8%B2%A1%E7%B6%93/',
                 'article':'.piece',
                 'title':'h3 a',
                 'url':'h3 a',
                 'category':'fin'},
                {'website':'https://technews.tw/category/%E5%9C%8B%E9%9A%9B%E8%B2%BF%E6%98%93/%E5%9C%8B%E9%9A%9B%E9%87%91%E8%9E%8D',
                 'article':'article',
                 'category':'fin'},
                {'website':'https://hk.prnasia.com/story/industry/FIN-2.shtml',
                 'article':'.presscolumn',
                 'category':'fin'},
                {'website':'https://www.hket.com',
                 'article':'.listing-content-container',
                 'category':'fin'},
                {'website':'http://big5.ftchinese.com/channel/economy.html',
                 'article':'.item-container',
                 'category':'fin'},
                {'website':'http://big5.ftchinese.com/channel/markets.html',
                 'article':'.item-container',
                 'category':'fin'},
                {'website':'http://big5.ftchinese.com/channel/business.html',
                 'article':'.item-container',
                 'category':'fin'},
                {'website':'https://www.worldjournal.com/topic/%E7%BE%8E%E5%9C%8B%E8%B2%A1%E7%B6%93',
                 'article':'article',
                 'title':'h2 a',
                 'url':'h2 a',
                 'category':'fin'},
                {'website':'https://www.cna.com.tw/list/aie.aspx',
                 'article':'#myMainList li',
                 'category':'fin'},
                {'website':'https://www.wantgoo.com/news/list/index',
                 'article':'.list-lite li',
                 'category':'fin'},
                {'website':'http://www.aastocks.com/tc/stocks/news/aafn',
                 'article':'.newshead4',
                 'category':'fin'},

                 ]


    def getNewsFromWebsite(self, website):
        news = []
        
        user_agent = {'User-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.81 Safari/537.36'}
        res = requests.get(website['website'], headers=user_agent)
        res.encoding = 'utf-8'
        res = res.text

        f = open('x.html', 'w')
        f.write(res)
        f.close()

        website['title'] = website.get('title', 'a')
        website['url'] = website.get('url', 'a')
        website['category'] = website.get('category', 'it')

        soup = BeautifulSoup(res, 'html.parser')
        for article in soup.select(website['article']):
            data = {}
            data['title'] = article.select(website['title'])[0].text.strip()
            data['url'] = article.select(website['url'])[0].get('href')
            data['category'] = website['category']
            if not data['url'].startswith('http'):
                url = website['website'].split(':')[0] + '://' + website['website'].split('://')[1].split('/')[0]
                data['url'] = url + data['url']

            news.append(data)

        return news

    def getNews(self):
        articles = []
        for website in self.websites:
            try:
                articles += self.getNewsFromWebsite(website)
            except:
                pass

        return articles

    
    def uploadToServer(self, articles):
        for article in articles:
            article['passwd'] = UPLOAD_PASSWORD
            try:
                res = requests.post(ARTICLE_PLUS, data=article).text
                print(f'{article["title"]} / {res}')
            except:
                pass

        


if __name__ == '__main__':

    bot = NewsBot()
    articles = bot.getNews()
    bot.uploadToServer(articles)

