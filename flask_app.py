# A very simple Flask Hello World app for you to get started with...

from datetime import datetime, timedelta
import os

from flask import Flask, render_template, request, jsonify
from flask_bootstrap import Bootstrap
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import and_
from flask_caching import Cache


app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = f'sqlite:////{os.getcwd()}/test.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
cache = Cache(config={'CACHE_TYPE': 'simple'})
cache.init_app(app)
db = SQLAlchemy(app)
bootstrap = Bootstrap(app)


class Article(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(300), unique=True, nullable=False)
    url = db.Column(db.String(300), unique=True, nullable=False)
    category = db.Column(db.String(10), nullable=False)
    date = db.Column(db.DateTime, default=datetime.now)

#@app.route('/create_db')
#def cread_db():
#    db.create_all()
#    return 'create ok'

@app.template_filter('formatdatetime')
def filte_datetime(value, format="%Y-%m-%d %H:%M"):
    if value is None:
        return None
    return ( value + timedelta(hours=8) ).strftime(format)


@app.template_filter('website')
def filte_website(value):
        websites = {
                'segmentfault.com':'Segmentfault',
                'csdn.net':'CSDN',
                'gitee.com':'碼雲',
                'techbridge.cc':'TechBridge',
                'jaceju.net':'jaceju',
                'ithome.com.tw':'iThome',
                'inside.com.tw':'硬塞的網路趨勢觀察',
                'buzzorange.com':'報橘',
                'fundebug.com':'FunDebug',
                'bnext.com.tw':'數位時代',
                'ifanr.com':'愛范兒',
                'jobbole.com':'伯樂在線',
                'oschina.net':'中文開源技術交流社區',
                'cnblogs.com':'博客園',
                'codeceo.com':'碼農網',
                'toutiao.io':'今日頭條',
                'iteye.com':'ITeye',
                'cw.com.tw':'天下雜誌',
                'yahoo.com':'Yahoo新聞',
                'bbc.com':'BBC',
                'epochtimes.com':'大紀元時報',
                'ettoday.net':'ETtoday',
                'technews.tw':'科技新報',
                'hk.prnasia.com':'美通社',
                'hket.com':'香港經濟日報',
                'ftchinese.com':'金融時報',
                'worldjournal.com':'世界日報',
                'cna.com.tw':'中央社CNA',
                'wantgoo.com':'玩股網',
                'aastocks.com':'阿思達克財經網'}
        name = [ websites[website] for website in websites if value.split('//')[1].split('/')[0].endswith(website)]

        if name:
            return name[0]
        else:
            return 'No'



@app.route('/add_article', methods=['POST'])
def add_article():
    title = request.form.get('title', None)
    url = request.form.get('url', None)
    category = request.form.get('category', None)
    passwd = request.form.get('passwd', None)

    if passwd == '':
        try:
            article = Article(title=title, url=url, category=category)
            db.session.add(article)
            db.session.commit()
        except:
            return 'add article fail'
        return 'add article sucess'
    else:
        return 'password fail'



@app.route('/')
@app.route('/it/')
@cache.cached(timeout=300, query_string=True)
def view_it_page():
    page = request.args.get('page', 1, type=int)
    pagination = Article.query.filter(Article.category=='it').order_by(Article.date.desc()).paginate(page, per_page=100, error_out=False)
    articles = pagination.items
    return render_template('it.html', pagination=pagination, articles=articles)

@app.route('/fin/')
@cache.cached(timeout=300, query_string=True)
def view_fin_page():
    page = request.args.get('page', 1, type=int)
    pagination = Article.query.filter(Article.category=='fin').order_by(Article.date.desc()).paginate(page, per_page=100, error_out=False)
    articles = pagination.items
    return render_template('fin.html', pagination=pagination, articles=articles)

@app.route('/search/')
def view_search():
    keyword = request.args.get('keyword', None, type=str)

    if keyword:
        keyword = [f'%{word}%' for word in keyword.split(' ') if word != '']

    if keyword:
        rules = and_(*[Article.title.ilike(word) for word in keyword])
        page = request.args.get('page', 1, type=int)
        pagination = Article.query.filter(rules).order_by(Article.date.desc()).paginate(page, per_page=100, error_out=False)
        articles = pagination.items
        return render_template('search.html', pagination=pagination, articles=articles, keyword=' '.join(keyword))
    return render_template('search.html')

@app.route('/about')
def view_about():
    webs = {
            'https://segmentfault.com': 'Segmentfault',
            'https://www.csdn.net':'CSDN',
            'https://blog.techbridge.cc':'TechBridge',
            'https://jaceju.net':'jaceju',
            'https://www.ithome.com.tw':'iThome',
            'https://www.inside.com.tw':'硬塞的網路趨勢觀察',
            'https://buzzorange.com':'報橘',
            'https://blog.fundebug.com':'FunDebug',
            'https://www.bnext.com.tw':'數位時代',
            'https://www.ifanr.com':'愛范兒',
            'http://blog.jobbole.com':'伯樂在線',
            'https://www.oschina.net':'中文開源技術交流社區',
            'https://www.cnblogs.com':'博客園',
            'http://www.codeceo.com':'碼農網',
            'https://toutiao.io':'今日頭條',
            'https://www.iteye.com':'ITeye',
            'https://www.cw.com.tw':'天下雜誌',
            'https://tw.news.yahoo.com':'Yahoo新聞',
            'https://www.bbc.com':'BBC',
            'http://www.epochtimes.com':'大紀元時報',
            'https://www.ettoday.net':'ETtoday',
            'https://technews.tw':'科技新報',
            'https://hk.prnasia.com':'美通社',
            'https://www.hket.com':'香港經濟日報',
            'http://big5.ftchinese.com':'金融時報',
            'https://www.worldjournal.com':'世界日報',
            'https://www.cna.com.tw':'中央社CNA',
            'https://www.wantgoo.com':'玩股網',
            'http://www.aastocks.com':'阿思達克財經網'}
    websites = [{'url':url, 'name':webs[url]} for url in webs]
    return render_template('about.html', websites=websites)


@app.errorhandler(404)
@cache.cached(timeout=300)
def page_not_found(e):
    return render_template('404.html'), 404


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000, debug=True)
