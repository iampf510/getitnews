from logging import getLogger

from flask import current_app, request
from flask_restful import Resource

from .misc import filte_date

logger = getLogger(__name__)


class APIView(Resource):
    model = None
    schema = None
    # pre_load = None
    
class APIListView(Resource):
    order_by = None
    
    def get(self, **kwargs):
        PERPAGE = current_app.config['PERPAGE']
        page = request.args.get('page', 1, type=int)
        pagination = (self.model.query
                      .filter_by(**kwargs)
                      .order_by(self.order_by)
                      .paginate(page, 
                                per_page=PERPAGE, 
                                error_out=False)
                    )
        objs = pagination.items
        datas = self.schema.dump(objs)

        # if self.pre_load:
        #     self.pre_load(datas)

        
        return datas
        
