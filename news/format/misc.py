from datetime import datetime, timedelta
from logging import getLogger

logger = getLogger(__name__)


def filte_date(value, format = "%Y-%m-%d %H:%M"):
    try:
        value = datetime.fromisoformat(value)
    except:
        return None
    return (value + timedelta(hours=8)).strftime(format)
