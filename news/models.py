from datetime import datetime

from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()

class Article(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.Text, unique=True, nullable=False)
    url = db.Column(db.Text, unique=True, nullable=False)
    category = db.Column(db.String(10), nullable=False)
    date = db.Column(db.DateTime, default=datetime.now)


class Rule(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    website = db.Column(db.Text, nullable=False)
    article = db.Column(db.Text, nullable=False)
    title = db.Column(db.Text, nullable=False)
    url = db.Column(db.Text, nullable=False)
    category = db.Column(db.String(10), nullable=False)
    name = db.Column(db.Text, unique=True, nullable=False)

class Websites(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    url = db.Column(db.Text, nullable=False)
    name = db.Column(db.Text, unique=True, nullable=False)
    
