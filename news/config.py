import os
import random

from dotenv import load_dotenv

load_dotenv()

def str2bool(v:str) -> bool:
    return str(v).lower() in ('true', '1')

def str2int(v) -> int:
    return int(v) if isinstance(v,int) else 0

class DefaultConfig:
    DEBUG = str2bool(os.getenv("DEBUG", False))
    SECRET_KEY = os.getenv("SECERT_KEY")
    UPLOAD_PASSWORD = os.getenv("UPLOAD_PASSWORD")
    PERPAGE = int(os.getenv("PERPAGE", 100))

    # URL = postgresql://{user}:{passwd}@{host}:{port}/{db}
    # SQLALCHEMY_DATABASE_URL = os.getenv("DB_URL")
    DB_HOST = os.getenv('DB_HOST')
    DB_PORT = os.getenv('DB_PORT')
    DB_NAME = os.getenv('DB_NAME')
    DB_USER = os.getenv('DB_USER')
    DB_PASSWORD = os.getenv('DB_PASSWORD')
