#!/usr/bin/env python
import datetime
import time
import json
import requests
import traceback
from bs4 import BeautifulSoup

from config import WEBSITE_URL, ARTICLE_PLUS, UPLOAD_PASSWORD

def GET_CSDN():
        user_agent = {'User-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.81 Safari/537.36'}
        session = requests.Session()
        session.get('https://www.csdn.net/', headers=user_agent, timeout=30)
        rsp = session.get('https://www.csdn.net/api/articles?type=more&category=home', timeout=30).json()

        news = []

        for article in rsp['articles']:
            data = {}
            data['title'] = article['title']
            data['url'] = article['url']
            data['category'] = 'it'
            news.append(data)
        return news
    

class NewsBot:
    def __init__(self, websites, website_url, article_plus, upload_password):
        self.websites = websites
        self.website_url = website_url
        self.article_plus = article_plus
        self.upload_password = upload_password


    def getNewsFromWebsite(self, website):
        news = []

        if 'method' in website:
            news = website['method']()    
        
        else:
            user_agent = {'User-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.81 Safari/537.36'}
            res = requests.get(website['website'], headers=user_agent, timeout=30)
            res.encoding = 'utf-8'
            res = res.text

            #f = open('x.html', 'w')
            #f.write(res)
            #f.close()

            website['title'] = website.get('title', 'a')
            website['url'] = website.get('url', 'a')
            website['category'] = website.get('category', 'it')
            website['article'] = website.get('article', 'article')

            soup = BeautifulSoup(res, 'html.parser')
            for article in soup.select(website['article']):
                try:
                    data = {}
                    data['title'] = article.select(website['title'])[0].text.strip()
                    if website['url'] == 'self':
                        data['url'] = article.get('href')
                    else:
                        data['url'] = article.select(website['url'])[0].get('href')
                    data['category'] = website['category']
                    if not data['url'].startswith('http'):
                        url = website['website'].split(':')[0] + '://' + website['website'].split('://')[1].split('/')[0]
                        data['url'] = url + data['url']

                    news.append(data)
                except Exception as exp:
                    pass

        return news

    def getNews(self):
        articles = []
        for website in self.websites:
            try:
                new_articles = self.getNewsFromWebsite(website)
                if len(new_articles) == 0:
                    print(f'{website["website"]} - {len(new_articles)}')
                articles += new_articles
            except Exception as exp:
                pass

        return articles

    
    def uploadToServer(self, articles):
        try:
            old_articles = requests.get(self.website_url + 'get_news/' + str(len(articles)), timeout=60).json()
            articles = [ article for article in articles if article['url'] not in old_articles ]
            for i in range( int(len(articles)/100)+1):
                res = requests.post(self.article_plus, data={'datas':json.dumps(articles[i*100:(i+1)*100]), 'passwd':self.upload_password}, timeout=300).text
                #res = requests.post(self.article_plus, data=article, timeout=30).text
        except:
            old_articles = []
            pass

        


if __name__ == '__main__':
    websites1 = [
            {'website':'https://segmentfault.com/blogs',
             'article':'.content',
             'title':'.title'},
            {'website':'https://www.csdn.net',
             'article':'.list_con',
             'title':'h2 a',
             'url':'h2 a',
             'method':GET_CSDN},
            {'website':'https://blog.techbridge.cc',
             'article':'.post-preview'},
            {'website':'https://www.ithome.com.tw',
             'article':'.title'},
            {'website':'https://www.inside.com.tw',
             'article':'.post_title'},
            {'website':'https://buzzorange.com/techorange',
             'article':'article',
             'title':'h4 a',
             'url':'h4 a'},
            {'website':'https://blog.fundebug.com',
             'article':'article',
             'title':'h1 a',
             'url':'h1 a'},
            {'website':'https://www.bnext.com.tw',
             'article':'.MobileBox',
             'title':'.three-line-text'},
            {'website':'https://www.ifanr.com',
             'article':'.article-item',
             'title':'h3 a',
             'url':'h3 a'},
            {'website':'https://www.oschina.net',
             'article':'.item'},
            {'website':'https://www.cnblogs.com',
             'article':'.post-item',
             'title':'.post-item-text a',
             'url':'.post-item-text a'},
            {'website':'http://www.codeceo.com',
             'article':'article',
             'title':'h3 a',
             'url':'h3 a'},
            {'website':'https://toutiao.io',
             'article':'.post',
             'title':'h3 a',
             'url':'h3 a'},
            {'website':'https://www.iteye.com',
             'article':'.title'},
            {'website':'https://www.cw.com.tw/masterChannel.action?idMasterChannel=8',
             'article':'.article',
             'title':'h3 a',
             'url':'h3 a',
             'category':'fin'},
            {'website':'https://tw.news.yahoo.com/finance',
             'article':'.Cf',
             'title':'h3 a',
             'url':'h3 a',
             'category':'fin'},
            {'website':'https://www.bbc.com/zhongwen/trad/business',
             'article':'.lx-stream-post',
             'category':'fin'},
            {'website':'http://www.epochtimes.com/b5/news420.htm',
             'article':'.posts',
             'category':'fin'},
            {'website':'https://www.ettoday.net/news/focus/%E8%B2%A1%E7%B6%93/',
             'article':'.piece',
             'title':'h3 a',
             'url':'h3 a',
             'category':'fin'},
            {'website':'https://technews.tw/category/%E5%9C%8B%E9%9A%9B%E8%B2%BF%E6%98%93/%E5%9C%8B%E9%9A%9B%E9%87%91%E8%9E%8D',
             'article':'article',
             'category':'fin'},
            {'website':'https://hk.prnasia.com/story/industry/FIN-2.shtml',
             'article':'.presscolumn',
             'category':'fin'},
            {'website':'https://www.hket.com',
             'article':'.listing-content-container',
             'category':'fin'},
            {'website':'http://big5.ftchinese.com/channel/economy.html',
             'article':'.item-container',
             'category':'fin'},
            {'website':'http://big5.ftchinese.com/channel/markets.html',
             'article':'.item-container',
             'category':'fin'},
            {'website':'http://big5.ftchinese.com/channel/business.html',
             'article':'.item-container',
             'category':'fin'},
            {'website':'https://www.worldjournal.com/wj/cate/breaking',
             'article':'.subcate-list__link',
             'category':'fin'},
            {'website':'https://www.cna.com.tw/list/aie.aspx',
             'article':'#jsMainList li',
             'category':'fin'},
            {'website':'https://www.wantgoo.com/news/list/index',
             'article':'.list-lite li',
             'category':'fin'},
            {'website':'http://www.aastocks.com/tc/stocks/news/aafn',
             'article':'.newshead4',
             'category':'fin'},
            {'website':'https://www.freebuf.com/',
             'article':'.article-item'},
			{'website':'https://bitnance.vip/',
			'category':'coin',
			'name':'幣特財經',
			'article':'.item_news',
			'title':'.title',
			'url':'self'},
			{'website':'https://blockcast.it/',
			'category':'coin',
			'name':'區塊客',
			'title':'.jeg_post_title'},
			{'website':'https://www.grenade.tw/',
			'category':'coin',
			'name':'GRENADE',
			'title':'.elementor-post__title',
			'url':'h3 a'},
			{'website':'https://www.0xmag.asia/',
			'category':'coin',
			'name':'區塊誌',
			'title':'h2',
			'url':'h2 a'},
			{'website':'https://zombit.info/',
			'category':'coin',
			'name':'桑幣筆記',
			'title':'.jeg_post_title',
			'url':'.jeg_post_title a'},
			{'website':'http://www.blocktimes.tw/',
			'category':'coin',
			'name':'BlockTimes',
			'article':'.item',
			'title':'h3',
			'url':'self'},
			{'website':'https://bitcoinmagazine.com/',
			'category':'coin',
			'name':'BitcoinMagazine',
			'article':'.l-grid--item',
			'title':'.m-card--header-text',
			'url':'.m-card--header a'},
			{'website':'https://bitcoinist.com/',
			'category':'coin',
			'name':'Bitcoinist',
			'title':'.jeg_post_title',
			'url':'.jeg_post_title a'},
			{'website':'https://cointelegraph.com/',
			'category':'coin',
			'name':'CoinTelegraph',
			'title':'.post-card__title',
			'url':'.post-card__header a'},
			{'website':'https://blocktrend.substack.com/archive?sort=new',
			'category':'coin',
			'name':'區塊勢',
			'article':'.post-preview',
			'title':'.post-preview-content',
			'url':'.post-preview-content a'},
			{'website':'https://www.tuoluocaijing.cn/',
			'category':'coin',
			'name':'陀螺財經',
			'article':'.item',
			'title':'.intro a',
			'url':'.intro a'},
			{'website':'https://www.blocktempo.com/',
			'category':'coin',
			'name':'動區動驅',
			'title':'.jeg_post_title',
			'url':'.jeg_post_title a'},
			{'website':'https://cointmr.com/',
			'category':'coin',
			'name':'明日幣圈',
			'title':'.penci__post-title',
			'url':'.penci__post-title a'},
			{'website':'https://hk.investing.com/news/latest-news',
			'category':'fin',
			'name':'Investing',
			'title':'.textDiv',
			'url':'.textDiv a'},
			{'website':'https://news.cnyes.com/tag/%E6%AF%94%E7%89%B9%E5%B9%A3?exp=a',
			'category':'coin',
			'name':'鉅亨-比特幣',
			'title':'h3'},
			{'website':'https://www.bitcoin86.com/',
			'category':'coin',
			'name':'比特幣資訊網',
			'article':'.m-news-item',
			'title':'h4'},
			{'website':'https://tags.finance.sina.com.cn/%E6%AF%94%E7%89%B9%E5%B8%81',
			'category':'coin',
			'name':'新浪-比特幣',
			'article':'.feeds_list li',
			'title':'h3'},
			{'website':'https://bishijie.com/',
			'category':'coin',
			'name':'幣世界',
			'article':'.newscontainer li',
			'title':'h3'},
			{'website':'https://www.chainnews.com/',
			'category':'coin',
			'name':'鏈聞',
			'article':'.feed-item',
			'title':'.feed-post-title',
			'url':'.feed-post-title a'},
			{'website':'http://m.jrj.com.cn/bc/',
			'category':'coin',
			'name':'金融界-區塊鏈',
			'article':'.pt5',
			'title':'li a',
			'url':'li a'},

             ]

    #bot = NewsBot(websites2, 'http://getmorenews.pythonanywhere.com/', 'http://getmorenews.pythonanywhere.com/add_article', '123456')
    #articles = bot.getNews()
    #bot.uploadToServer(articles)

    while 1:
        print('='*30)
        print(f'Start fetch {datetime.datetime.now()}')
        print('='*30)
        try:
            bot = NewsBot(websites1, WEBSITE_URL, ARTICLE_PLUS, UPLOAD_PASSWORD)
            articles = bot.getNews()
            bot.uploadToServer(articles)
        except Exception as exp:
            err = open('error', 'a')
            err.write(traceback.format_exc())
            err.close()
        time.sleep(15*60)







