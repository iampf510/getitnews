import logging

from config import DefaultConfig
from flask import Flask
# from flask_migrate import Migrate, MigrateCommand
# from flask_script import Manager
from flask_marshmallow import Marshmallow
from models import db


def get_db_uri(config):
    # URL = postgresql://{user}:{password}@{host}:{port}/{db_name}
    user = config['DB_USER']
    password = config['DB_PASSWORD']
    host = config['DB_HOST']
    port = config['DB_PORT']
    db_name = config['DB_NAME']

    return f'postgresql://{user}:{password}@{host}:{port}/{db_name}'
    

def create_app():
    app = Flask(__name__)
    app.config.from_object(DefaultConfig)

    logger = logging.getLogger('gunicorn.error')
    app.logger.handlers = logger.handlers
    app.logger.setLevel(logger.level)

    app.config['SQLALCHEMY_DATABASE_URI'] = get_db_uri(app.config)
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
    db.init_app(app)

    @app.route('/create_db')
    def create_db():
        db.create_all()
        return 'ok'

    
    ma = Marshmallow()
    ma.init_app(app)

    # migrate = Migrate(app, db)
    # manager = Manager(app)
    # manager.add_command('db', MigrateCommand)

    return app

def register_blueprint(app):
    from flask_restful import Api

    from api.article.views import ArticleListView
    # from api.article.views import bp as bp_article
    from api.website.views import bp as bp_website

    api = Api(app)

    # app.register_blueprint(bp_article, url_prefix='/api/article')
    app.register_blueprint(bp_website, url_prefix='/api/website')
    api.add_resource(ArticleListView, '/api/article/<category>')
    
    
    
