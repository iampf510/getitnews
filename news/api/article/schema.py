from logging import getLogger

from format.misc import filte_date
# from flask_marshmallow import Schema, fields, validate
from marshmallow import Schema, fields, post_dump, validate

logger = getLogger(__name__)


class ArticleListSchema(Schema):
    title = fields.Str()
    url = fields.Str(validate=validate.URL())
    date = fields.DateTime()

    @post_dump
    def clean_date(self, data, **kwargs):
        data['date'] = filte_date(data['date'])
        return data
    


