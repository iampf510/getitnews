from datetime import timedelta
from logging import getLogger

from flask import Blueprint, current_app, jsonify, request
from format.view import APIListView
from models import Article, db
from werkzeug import exceptions

from .schema import ArticleListSchema

logger = getLogger(__name__)

bp = Blueprint('article', __name__)



class ArticleListView(APIListView):
    model = Article
    schema = ArticleListSchema(many=True)
    order_by = Article.date.desc()



# @bp.route('/<category>', methods=['GET'])
# def get_articles(category):
#     page = request.args.get('page', 1, type=int)
#     pagination = Article.query.filter_by(category=category).order_by(Article.date.desc()).paginate(page, per_page=100, error_out=False)
#     articles = pagination.items
#     logger.debug(articles)
#     return jsonify(articles)
# 
# @bp.route('/', methods=['POST'])
# def add_article():
#     data = request.json
#     
#     title = data.get('title', None)
#     url = data.get('url', None)
#     category = data.get('category', None)
#     passwd = data.get('passwd', None)
# 
#     if not any([title, url, category, passwd]):
#         raise exceptions.BadRequest('Need [title,url,category,passwd]')
# 
#     if passwd == current_app.config['UPLOAD_PASSWORD']:
#         try:
#             article = Article(title=title, url=url, category=category)
#             db.session.add(article)
#             db.session.commit()
#         except:
#             db.session.rollback()
#             return jsonify({'result':'add article fail'})
#         return jsonify({'result':'finish'})
#     else:
#         raise exceptions.BadRequest('Upload password is wrong')
    
