from flask import Blueprint
from models import Rule
from format.view import APIView, APIListView

bp = Blueprint('website', __name__)

@bp.route('/test')
def test():
    return '123'
